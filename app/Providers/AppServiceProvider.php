<?php

namespace App\Providers;

use App\Cleint;
use Illuminate\Support\Str;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Specified key was too long error, Laravel News post:
        Schema::defaultStringLength(191);
        Resource::withoutWrapping();

        Cleint::created(function($client){
            $token = Str::random(60);
            $client->api_token = hash('sha256', $token);
            $client->save();
        });

        // if (!Collection::hasMacro('paginate')) {
        //     Collection::macro('paginate', 
        //         function ($perPage = 15, $page = null, $options = []) {
        //             $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        //             return (new LengthAwarePaginator(
        //                 $this->forPage($page, $perPage), $this->count(), $perPage, $page, $options))
        //                 ->withPath('');
        //         }
        //     );
        // }

    }
}
