<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class WineRecipe extends Model
{
    protected $table = "wine_recipes";
}
