<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Recipe extends Model
{
  	public function gallery()
	{
	    return $this->belongsToMany('App\Gallery');
	}

	public function wines()
	{
	    return $this->belongsToMany('App\Wine');
	}
}
