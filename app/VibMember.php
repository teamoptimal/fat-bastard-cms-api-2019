<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VibMember extends Model
{

    protected $table = "members";
    
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'mobile',
        'gender',
        'birth_date',
        'province',
        'city',
        'chardonnay',
        'rose',
        'merlot',
        'pinotage',
        'shiraz',
        'sauvignon_blanc',
        'cabernet_sauvignon',
        'the_golden_reserve',
        'facebook',
        'whatsapp',
        'snapchat',
        'none',
        'we_chat',
        'pinterest',
        'youtube',
        'instagram',
        'google_plus',
        'twitter',
        'subscribed',
        'subscribed_sms',
    ];
}
