<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Traits\EntityResponser;
use App\Traits\GalleryGenerator;

class ApiController extends Controller
{
    use ApiResponser;
}
