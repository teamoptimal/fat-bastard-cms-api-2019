<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
      protected function create(array $data)
    {
        $cart_content = Cart::content();

//        var_dump($cart_content);exit;

        foreach ($cart_content as $item) {
            $selectedProduct = Product::where('name',$item->name)->first();

            //var_dump($selectedProduct);

            if($selectedProduct->featured) {
                Cart::update($item->rowId, ['price' => $selectedProduct->special_price]);
            }
        }

        $society_list = 'LIST_ID_HERE';
    
        $everlytic_consumer = array (
            'name' => $data['first_name'],
            'lastname' => $data['last_name'],
            'gender' => "male",
            'bith_date' => "1990-03-20",
            'email' => $data['email'],
            'mobile' => $data['cell_number'],
            'on_duplicate' => "update",
            'list_id' =>
                array ($society_list => 'subscribed'),
        );

        $json = json_encode($everlytic_consumer, JSON_FORCE_OBJECT);

        $url = 'http://mailers.optimalonline.co.za/api/2.0/contacts';
        $method = 'POST';
        $cSession = curl_init();
        $headers = array();
        $auth = base64_encode("FAT" . ':' . "S0SSpyEkomSturq0PSmjpOMr7qndnTfP_4"); // UPDATE ME!!!!!!!!!!!!!!
        $headers[] = 'Authorization: Basic ' . $auth;
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_HEADER, false);
        curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        curl_setopt($cSession, CURLOPT_POSTFIELDS, $json);
        $headers[] = 'Content-Type: application/json';
        curl_setopt($cSession, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($cSession);
        // var_dump($result);exit;
        curl_close($cSession);

        // $oldMember = MemberData::where('email', $data['email'])->first();

        // $membership_number = isset($oldMember) ? $oldMember->code : User::generateMembershipNumber($data['last_name']);

        return User::create([
            // 'membership_number' => $membership_number,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'cell_number' => $data['cell_number'],
            'country' => $data['country'],
            'province' => $data['province'],
            'city' => $data['city'],
            'suburb' => $data['suburb'],
            'street_number' => $data['street_number'],
            'route' => $data['route'],
            'postalcode' => $data['postalcode'],
        ]);
    }
}
