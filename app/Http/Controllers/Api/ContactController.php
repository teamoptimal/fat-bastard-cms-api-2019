<?php

namespace App\Http\Controllers\Api;

// use App\Consumer;
// use App\Mail\SignUp;
// use App\Mail\ContactRequest;
use App\Mail\GeneralEnquiry;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Mail;

class ContactController extends ApiController
{
    public function subscribe($id, $list_id){
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:consumers',
            'location' => 'required',
            // 'g-recaptcha-response' => 'required|recaptcha'
        ];

        $this->validate($request, $rules);

        $consumer = Consumer::create($request->all());
        $type = $request->type;
        $list_sales = '102330';
        $list_rentals = '102331';
        
        if($type == 'for-sale') {
            $list_id = $list_sales;
        }else {
            $list_id = $list_rentals;
        }

        $everlytic_consumer = array (
            'name' => $consumer->first_name,
            'lastname' => $consumer->last_name,
            'gender' => "male",
            'bith_date' => "1990-03-20",
            'email' => $consumer->email,
            'mobile' => $consumer->contact,
//                    'on_duplicate' => "update",
            'list_id' =>
                array ($list_id => 'subscribed'),
        );

        $json = json_encode($everlytic_consumer, JSON_FORCE_OBJECT);

        $url = 'https://optimalonline.everlytic.net/api/2.0/contacts';
        $method = 'POST';
        $cSession = curl_init();
        $headers = array();
        $auth = base64_encode("kyle@optimalonline.co.za0" . ':' . "9mnS5KlxGwUZ9NbNC1Qm5GtLUOUbTsDy");
        $headers[] = 'Authorization: Basic ' . $auth;
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_HEADER, false);
        curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        curl_setopt($cSession, CURLOPT_POSTFIELDS, $json);
        $headers[] = 'Content-Type: application/json';
        curl_setopt($cSession, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($cSession);

        curl_close($cSession);

        $contact = null;

        if (isset($request->contact) && $request->contact !== null) {
            $contact = $request->contact;
        }

        $person = collect([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'contact' => $contact,
            'location' => $request->location,
        ]);

        retry(5, function() use ($person) {
            Mail::to($person['email'])->send(new SignUp($person));
        }, 100);

        return redirect()->route('thankYou', $type);
    }

    public function unsubscribe($id, $list_id){
        $everlytic_consumer = array (
            'list_id' => $list_id,
            'contact_id' => $id,
            'email_status' => 'unsubscribed',
        );

        $json = json_encode($everlytic_consumer, JSON_FORCE_OBJECT);

        $url = 'http://mailers.optimalonline.co.za/api/2.0/list_subscriptions/:list_id';
        $method = 'POST';
        $cSession = curl_init();
        $headers = array();
        $auth = base64_encode("kyle@optimalonline.co.za0" . ':' . "9mnS5KlxGwUZ9NbNC1Qm5GtLUOUbTsDy");
        $headers[] = 'Authorization: Basic ' . $auth;
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_HEADER, false);
        curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        curl_setopt($cSession, CURLOPT_POSTFIELDS, $json);
        $headers[] = 'Content-Type: application/json';
        curl_setopt($cSession, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($cSession);
        curl_close($cSession);

//        var_dump($result);exit;
    }

    public function generalEnquiry(Request $request)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            // 'g-recaptcha-response' => 'required|recaptcha'
        ];

        $this->validate($request, $rules);

        $person = collect([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'message' => $request->message,
        ]);

        // retry(5, function() use ($person) {
        //     Mail::to("info@fatbastardwine.co.za")->cc("kyle@optimalonline.co.za")->send(new GeneralEnquiry($person));
        // }, 100);

        Mail::to("riekie@robertsonwinery.co.za")->cc(["kyle@optimalonline.co.za","info@fatbastardwine.co.za"])->send(new GeneralEnquiry($person));

        //return redirect()->back()->with('mail_sent', 'Mail has been sent');
        if( count(Mail::failures()) > 0 ) {
		   	return response()->json(Mail::failures());
		} else {
		    return response()->json('success');
		}
    }
}
