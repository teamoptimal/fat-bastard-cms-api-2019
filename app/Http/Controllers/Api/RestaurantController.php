<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

use App\Restaurant;
use App\Http\Resources\Restaurant as RestaurantResource;

class RestaurantController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Pages
        $restaurants = Restaurant::paginate(20);

        // Return collection of pages
        return RestaurantResource::collection($restaurants);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $restaurant = Restaurant::where('slug', $slug)->first();

        // Return page as resource
        return new RestaurantResource($restaurant);
    }

}
