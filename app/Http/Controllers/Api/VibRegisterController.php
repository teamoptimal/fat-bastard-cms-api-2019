<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\VibMember;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationMail;
use App\Mail\WelcomMail;
use Illuminate\Support\Str;

class VibRegisterController extends Controller
{
    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'mobile' => 'required|string',
            'birth_date' => 'required|string',
            'province' => 'required|string',
            'city' => 'required|string',
        ]);

        if($validator->fails()){
            return response($validator->errors(), 403);
        }

        $record = new VibMember;
        $record->cabernet_sauvignon = $request->cabernet_sauvignon;
        $record->the_golden_reserve = $request->the_golden_reserve;
        $record->sauvignon_blanc = $request->sauvignon_blanc;
        $record->first_name = $request->first_name;
        $record->last_name = $request->last_name;
        $record->email = $request->email;
        $record->mobile = $request->mobile;
        $record->gender = $request->gender;
        $record->birth_date = $request->birth_date;
        $record->province = $request->province;
        $record->city = $request->city;
        $record->chardonnay = $request->chardonnay;
        $record->rose = $request->rose;
        $record->merlot = $request->merlot;
        $record->chenin_blanc = $request->chenin_blanc;
        $record->pinotage = $request->pinotage;
        $record->shiraz = $request->shiraz;
        $record->facebook = $request->facebook;
        $record->whatsapp = $request->whatsapp;
        $record->snapchat = $request->snapchat;
        $record->we_chat = $request->we_chat;
        $record->pinterest = $request->pinterest;
        $record->youtube = $request->youtube;
        $record->instagram = $request->instagram;
        $record->google_plus = $request->google_plus;
        $record->twitter = $request->twitter;
        $record->subscribed = $request->subscribed;
        $record->subscribed_sms = $request->subscribed_sms;
        $record->save();

        Mail::to("info@fatbastardwine.co.za")->bcc('kyle@optimalonline.co.za')->send(new RegistrationMail($record));
        Mail::to($request->email)->send(new WelcomMail($record));

        $json = '{ 
            "name":"'.$request->first_name.'",
            "lastname":"'.$request->last_name.'",
            "birth_date":"'.$request->birth_date.'",
            "email":"'.$request->email.'",
            "mobile": "'.$request->mobile.'",
            "gender": "'.$request->gender.'",
            "country": "South Africa",
            "province": "'.$request->province.'",
            "city": "'.$request->city.'",
            "on_duplicate":"update",
            "list_id":{"417461":"subscribed"}
        }';

        $url = 'https://optimalonline.everlytic.net/api/2.0/contacts';
        $method = 'POST';
        $apikey = env('EVERLYTIC_API_KEY', '');
        $username = 'fatbastard';
        $cSession = curl_init();
        $headers = array();
        $auth = base64_encode($username . ':' . $apikey);
        $headers[] = 'Authorization: Basic ' . $auth;
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_HEADER, false);
        curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        curl_setopt($cSession, CURLOPT_POSTFIELDS, $json);
        $headers[] = 'Content-Type: application/json';
        curl_setopt($cSession, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($cSession);
        curl_close($cSession);
        //everlytic::end

        return response($record);
    }
}
