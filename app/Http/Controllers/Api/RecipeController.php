<?php

namespace App\Http\Controllers\Api;

use App\Wine;
use App\Recipe;
use App\WineRecipe;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\Recipe as RecipeResource;

class RecipeController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Pages
        $recipes = Recipe::orderBy('created_at', 'desc')->paginate(20);

        // Return collection of pages
        return RecipeResource::collection($recipes);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $recipe = Recipe::where('slug', $slug)->get();
        $recipeArr = $recipe->toArray();

        $recipe_wine = WineRecipe::where('recipe_id', $recipe[0]->id)->first();
        $wine = Wine::where('id', $recipe_wine->wine_id)->first();

        $recipeArr[0]['wine'] = $wine;

        // Return page as resource
        return new RecipeResource($recipeArr);
    }

}
