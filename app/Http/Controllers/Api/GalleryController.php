<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

use App\Gallery;
use App\Http\Resources\Gallery as GalleryResource;

class GalleryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Pages
        $galleries = Gallery::paginate(10);

        // Return collection of pages
        return GalleryResource::collection($galleries);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($page_id, $type)
    {
        $gallery = Gallery::where('page_id', $page_id)->where('type', $type)->first();

        if(!isset($gallery)){
            return $this->errorResponse('Could not find a gallery with the specified ID', 404);
        }

        // Return page as resource
        return new GalleryResource($gallery);
    }

}
