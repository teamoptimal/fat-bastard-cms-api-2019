<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

use App\Page;
use App\Http\Resources\Page as PageResource;

class PageController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        // Get Pages
        $pages = Page::all();

        return $this->showAll($pages);
        // Return collection of pages
        return PageResource::collection($pages2);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($url)
    {
        $page = Page::where('url', $url)->first();

        if(!isset($page)){
            return $this->errorResponse('Could not find a page with the specified URL: ' . $url, 404);
        }

        // Return page as resource
        return new PageResource($page);
    }

}
