<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

use App\Video;
use App\Http\Resources\Video as VideoResource;

class VideoController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Pages
        $videos = Video::paginate(10);

        // Return collection of pages
        return VideoResource::collection($videos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video = Video::findOrFail($id);

        // Return page as resource
        return new VideoResource($video);
    }

}
