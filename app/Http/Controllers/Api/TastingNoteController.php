<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

use App\TastingNote;
use App\Http\Resources\TastingNote as TastingNoteResource;

class TastingNoteController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Pages
        $tasting_notes = TastingNote::all();

        // Return collection of pages
        return TastingNoteResource::collection($tasting_notes);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tasting_note = TastingNote::findOrFail($id);

        // Return page as resource
        return new TastingNoteResource($tasting_note);
    }

}
