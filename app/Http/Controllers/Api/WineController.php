<?php

namespace App\Http\Controllers\Api;

use App\Wine;
use App\Recipe;
use App\WineRecipe;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\Wine as WineResource;

class WineController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Pages
        $wines = Wine::paginate(10);

        foreach ($wines as $key => $wine) {
            $wineArr = $wine->toArray();

            $recipe_wine = WineRecipe::where('wine_id', $wine->id)->first();
           
            if($recipe_wine) {
                $recipes = Recipe::where('id', $recipe_wine->recipe_id)->whereNull('deleted_at')->get();
                $wineArr['recipes'] = $recipes;
            }

            $wineArr['tastingNotes'] = $wine->tasting_notes;

            $wines[$key] = $wineArr;
        }

        // Return collection of pages
        return WineResource::collection($wines);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $wine = Wine::where('slug', $slug)->get();

        if(!isset($wine)){
            return $this->errorResponse('Could not find a wine with the specified URL: ' . $url, 404);
        }

        $wineArr = $wine->toArray();

        $recipe_wine = WineRecipe::where('wine_id', $wine[0]->id)->first();
        $recipes = Recipe::where('id', $recipe_wine->recipe_id)->whereNull('deleted_at')->get();

        $wineArr[0]['recipes'] = $recipes;
        $wineArr[0]['tastingNotes'] = $wine[0]->tasting_notes;

        // Return page as resource
        return new WineResource($wineArr);
    }

}
