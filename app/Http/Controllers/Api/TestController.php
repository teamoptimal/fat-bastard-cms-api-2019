<?php

namespace App\Http\Controllers\Api;

use App\Cleint;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;


class TestController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getReferrer($refered_url){
        $before = Str::after($refered_url, 'http://');
        $after = Str::before($before, '/');
        return $after;
    }

    public function index()
    {
       
        $refered_url = 'http://localhost:3000/streams/new';
        $domain = $this->getReferrer($refered_url);
        $clients = Cleint::all()->pluck('domain');

        
        if(in_array($domain, $clients->toArray())) {
            return response()->json($clients);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($page_id, $type)
    {
        
    }

}
