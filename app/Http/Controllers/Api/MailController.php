<?php

namespace App\Http\Controllers\Api;

use App\Mail\GeneralEnquiry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
  public function sendMail(Request $request){
  	$rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email',
        'message' => 'required',
        // 'g-recaptcha-response' => 'required|recaptcha'
    ];

    $this->validate($request, $rules);

    $contact = null;

    if (isset($request->mobile) && $request->mobile !== null) {
        $contact = $request->mobile;
    }

    $to_address = 'info@fatbastardwine.co.za';

    $person = collect([
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'email' => $request->email,
        'contact' => $contact,
        'message' => $request->message,
        'address' => $to_address
    ]);

    // retry(5, function() use ($person, $to_address) {
    //     Mail::to("riekie@robertsonwinery.co.za")->cc("info@fatbastardwine.co.za")->send(new GeneralEnquiry($person));
    // }, 100);

    retry(5, function() use ($person, $to_address) {
        Mail::to("hannes@optimalonline.co.za")->send(new GeneralEnquiry($person));
    }, 100);

    return response()->json('success');          
  }
}
