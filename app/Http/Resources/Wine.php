<?php

namespace App\Http\Resources;


use App\Recipe;
use App\WineRecipe;
use Illuminate\Http\Resources\Json\JsonResource;

class Wine extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);

        // return [
        //     'id' => $this->id,
        //     'url' => $this->title,
        //     'title' => $this->title,
        //     'subtitle' => $this->subtitle
        // ];
    }

    // public function with($request) {
    //     $tasting_notes = $this->tasting_notes;
    
    //     $recipe_wine = WineRecipe::where('wine_id', $this->id)->first();
    //     $recipes = Recipe::where('id', $recipe_wine->recipe_id)->get();

    //     return [
    //         'tasting_notes' => $tasting_notes,
    //         'recipes' => $recipes
    //     ];
    // }
}
