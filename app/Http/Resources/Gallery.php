<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Gallery extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);

        // return [
        //     'id' => $this->id,
        //     'url' => $this->title,
        //     'title' => $this->title,
        //     'subtitle' => $this->subtitle
        // ];
    }

    public function with($request) {
        $images = $this->images;
        return [
            'images' => $images
        ];
    }
}
