<?php

namespace App\Http\Resources;

use App\Wine;
use App\WineRecipe;
use Illuminate\Http\Resources\Json\JsonResource;

class Recipe extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);

        // return [
        //     'id' => $this->id,
        //     'url' => $this->title,
        //     'title' => $this->title,
        //     'subtitle' => $this->subtitle
        // ];
    }

    // public function with($request) {
    //     $recipe_wine = WineRecipe::where('recipe_id', $this[0]->id)->first();
    //     $wine = Wine::where('id', $recipe_wine->wine_id)->first();

    //     return [
    //         'wine' => $wine
    //     ];
    // }
}
