<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;


trait ApiResponser
{
	protected function redirectAfterUpdate($route, $params, $message)
	{
		alert()->message($message)->autoclose(6000)->persistent("Close");
	   	return redirect()->route($route, $params);
	}

	protected function redirectAfterDelete($route, $params, $message)
	{
		alert()->message($message)->autoclose(6000)->persistent("Close");
	   	return redirect()->route($route, $params);
	}

	private function successResponse($data, $code)
	{
		 return response()->json($data, $code);
	}

	protected function errorResponse($message, $code)
	{
		 return response()->json(['error' => $message, 'code' => $code]);
	}

	protected function showAll(Collection $collection)
	{
		$collection = $this->filterData($collection);
		$collection = $this->sortData($collection);
		$collection = $this->paginate($collection);

		return collect($collection);
	}

	protected function showOne(Model $model, $code = 200)
	{
		return $this->successResponse(['data' => $model], $code);
	}

	protected function showMessage($message, $code = 200)
	{
		return $this->successResponse(['data' => $message], $code);
	}

	public function filterData(Collection $collection)
	{
		foreach (request()->query() as $attribute => $value) {
			if(isset($attribute, $value) && $attribute != 'sort_by' && $attribute != 'page'){
				$collection = $collection->where($attribute, $value);
			}
		}

		return $collection;
	}


	public function sortData(Collection $collection)
	{	
		if (request()->has('sort_by')) {
			$attribute = request()->sort_by;
			$sorted = $collection->sortBy($attribute);
			
			$collection = $sorted->values()->all();
		}
		return $collection;
	}

	public function paginate(Collection $collection)
	{
		$page = LengthAwarePaginator::resolveCurrentPage();

		$perPage = 5;

		$results = $collection->slice(($page - 1) * $perPage, $perPage)->values();

		$paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
			'path' => LengthAwarePaginator::resolveCurrentPath(),
		]);

		$paginated->appends(request()->all());

		return $paginated; 
	}
}