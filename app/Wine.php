<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Wine extends Model
{
    public function tasting_notes()
    {
        return $this->belongsToMany('App\TastingNote')->orderBy('title', 'DESC');
    }

   	public function recipes()
    {
        return $this->belongsToMany('App\Recipe')->whereNull('deleted_at');
    }
}
