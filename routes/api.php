<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['prefix' => 'v2', 'middleware' => ['apiauth:REMOTE_APP', 'cors']], function () {

    Route::post('vib-register', 'Api\VibRegisterController@register');

    Route::post('send-mail', 'Api\MailController@sendMail');

    // Test
    Route::resource('test', 'Api\TestController', ['only' => ['index', 'show']]);

     // Pages
    Route::resource('pages', 'Api\PageController', ['only' => ['index', 'show']]);

    // Wines
    Route::get('wines', 'Api\WineController@index');
    Route::get('wines/{slug}', 'Api\WineController@show');

    // Tasting Notes
    Route::resource('tasting-notes', 'Api\TastingNoteController', ['only' => ['index', 'show']]);

   // Restaurants
    Route::resource('restaurants', 'Api\RestaurantController', ['only' => ['index', 'show']]);

    // Videos
    Route::resource('videos', 'Api\VideoController', ['only' => ['index', 'show']]);

    // Videos
    Route::resource('recipes', 'Api\RecipeController', ['only' => ['index', 'show']]);

    // Galleries / Images
    Route::get('galleries', 'Api\GalleryController@index');
    Route::get('galleries/{page_id}/{type}', 'Api\GalleryController@show');

    Route::post('general-enquiry', 'Api\ContactController@generalEnquiry');
});

