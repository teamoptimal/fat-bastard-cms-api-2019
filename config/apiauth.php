<?php

return [
    'services' => [
        'REMOTE_APP' => [
            'token' => 'AWY97FKHW2',
            'tokenName' => 'api_token',

            'allowJsonToken' => true,
            'allowBearerToken' => true,
            'allowRequestToken' => true,
        ]
    ],
];
