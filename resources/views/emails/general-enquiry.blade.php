@component('mail::message')

From: {{ $person['first_name'] . ' ' .  $person['last_name'] }}<br>
Email: {{ $person['email'] }}<br>
@if(isset($person['contact']))
Phone: {{ $person['contact'] }}<br>
@endif

<hr>

{{ $person['message'] }}

Thanks,<br>
{{ $person['first_name'] . ' ' .  $person['last_name'] }}
@endcomponent
