@component('mail::message')
# Welcome to Helderberg Village!

Thank you for subscribing to our Newsletter. We look forward to creating relevant content for you.

{{ config('app.name') }}
@endcomponent


