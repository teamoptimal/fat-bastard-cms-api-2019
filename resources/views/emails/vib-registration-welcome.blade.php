

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="x-apple-disable-message-reformatting">
  <title></title>
  <!--[if mso]>
  <noscript>
    <xml>
      <o:OfficeDocumentSettings>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
  </noscript>
  <![endif]-->
  <style>
    table, td, div, h1, p {font-family: 'minion-pro', 'Crimson Text', serif;}
  </style>
</head>
<body style="margin:0;padding:0;">
  <table role="presentation" style="width:600px;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;margin: 0 auto;">
    <tr>
      <td align="center" style="padding:0;">
        <a href="https://www.fatbastardwine.co.za/" target="_blank">
            <img src="{{asset('img/test-03.gif')}}" alt="" style="height:auto;display:block;width: 100%;" />
        </a>
      </td>
    </tr>
    <tr>
        <td align="center" style="padding:0;">
            <p style="font-size: 25px; font-family: 'minion-pro', 'Crimson Text', serif;text-align: center;margin-top: 30px;color: #000;">Dear {{ $data['first_name'] }},</p>
            <!-- <p style="font-size: 25px">We are thrilled that you (a legend in your own right) have decided to start LIVING large and have joined our exclusive VIB (Very Important bastard) club!</p>
            <p style="font-size: 25px">Now you can enjoy all the benefits that all the other FAT bastard fans are receiving. Your membership grants you access to exciting FAT bastard competitions, special offers and enticing promotions at our VIB partner restaurants. This FAT bastard has your back!</p>
            <p style="font-size: 25px">Don’t forget to check out FAT bastard on Facebook, Twitter and Instagram and keep yourself up-to-date on all the happenings.</p>
            <p style="font-size: 25px">Here’s to living on the LARGE side of life, Mr. b</p> -->
        </td>
    </tr>
    <tr>
        <td align="center" style="padding:0;">
            <img src="{{asset('img/we-are-thrilled@2x.jpg')}}" alt="" width="600" style="height:auto;display:block;" />
        </td>
    </tr>
    <tr>
        <td align="center" style="padding:20px 0 0 0;">
            <a href="https://www.fatbastardwine.co.za/" target="_blank">
                <img src="{{asset('img/fat-bastard-logo@2x.jpg')}}" alt="" width="600" style="height:auto;display:block;" />
            </a>
        </td>
        
    </tr>
    <tr>
        <td align="center" style="padding:20px 0 0 0;">
            <img src="{{asset('img/follow-us@2x.jpg')}}" alt="" width="600" style="height:auto;display:block;" />
        </td>
    </tr>   
    <tr>
        <td align="center" style="padding:0;">
            <table>
                <tr>
                    <td style="padding: 20px 10px;">
                        <a href="https://www.instagram.com/fatbastardsa/" target="_blank">
                            <img src="{{asset('img/instagram-icon@2x.png')}}" alt="" width="40" style="height:auto;display:block;" />
                        </a>
                    </td>
                    <td style="padding: 20px 10px;">
                        <a href="https://www.facebook.com/FATbastardWineSA" target="_blank">
                            <img src="{{asset('img/facebook-icon@2x.png')}}" alt="" width="40" style="height:auto;display:block;" />
                        </a>
                    </td>
                    <td style="padding: 20px 10px;">
                        <a href="https://twitter.com/FATbastardSA" target="_blank">
                            <img src="{{asset('img/twitter-icon@2x.png')}}" alt="" width="40" style="height:auto;display:block;" />
                        </a>
                    </td>
                    <td style="padding: 20px 10px;">
                        <a href="https://www.youtube.com/@fatbastardwinessa290" target="_blank">
                            <img src="{{asset('img/youtube-icon@2x.png')}}" alt="" width="40" style="height:auto;display:block;" />
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr> 
    <tr>
        <td align="center" style="padding:0;">
            <img src="{{asset('img/enjoy-responsibly@2x.jpg')}}" alt="" width="600" style="height:auto;display:block;" />
        </td>
    </tr>   
    <tr>
        <td align="center" style="padding:0 0 60px 0;">
            <a href="https://www.fatbastardwine.co.za/" target="_blank">
                <img src="{{asset('img/sent-you-by@2x.jpg')}}" alt="" width="600" style="height:auto;display:block;" />
            </a>
        </td>
    </tr>   
      
  </table>
</body>
</html>

