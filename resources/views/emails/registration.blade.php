@component('mail::message')

You have a new entry from VIB Registration Form

@component('mail::table')
| Details: |          | 
| :------------- |:-------------| 
| Name      | {{ $data['first_name'] }}   {{ $data['last_name'] }}   |
| Email      | {{ $data['email'] }} |
| Mobile      | {{  $data['mobile']  }} |
| Gender      | {{  $data['gender']  }} |
| birth Date      | {{  $data['birth_date']  }} |
| Province      | {{  $data['province']  }} |
| City      | {{  $data['city']  }} |
@endcomponent

@component('mail::table')
| Update me via: |
| :------------- | 
@if($data['subscribed'])
| Email |
@endif
@if($data['subscribed_sms'])
| SMS |
@endif
@endcomponent

@component('mail::table')
| Interests: | 
| :------------- | 
@if($data['chardonnay'])
| Chardonnay |
@endif
@if($data['rose'])
| Rosé |
@endif
@if($data['cabernet_sauvignon'])
| Cabernet Sauvignon |
@endif
@if($data['merlot'])
| Merlot |
@endif
@if($data['pinotage'])
| Pinotage |
@endif
@if($data['shiraz'])
| Shiraz |
@endif
@if($data['sauvignon_blanc'])
| Sauvignon Blanc |
@endif
@if($data['chenin_blanc'])
| Chenin Blanc |
@endif
@if($data['the_golden_reserve'])
| The Golden Reserve |
@endif
@endcomponent

@component('mail::table')
| Social Media: | 
| :------------- | 
@if($data['facebook'])
| Facebook|
@endif
@if($data['whatsapp'])
| WhatsApp |
@endif
@if($data['snapchat'])
| Snapchat |
@endif
@if($data['we_chat'])
| WeChat |
@endif
@if($data['pinterest'])
| Pinterest |
@endif
@if($data['youtube'])
| YouTube |
@endif
@if($data['instagram'])
| Instagram |
@endif
@if($data['google_plus'])
| Google+ |
@endif
@if($data['twitter'])
| Twitter |
@endif

@endcomponent

@endcomponent
