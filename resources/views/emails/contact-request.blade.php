@component('mail::message')
# Contact Request

Greetings, my name is {{ ucwords($person['name']) }}, please contact me at {{ $person['email'] }}.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
